package ba.tracker.manager.gps;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 */
public class AboutActivity extends Activity {

    ImageView black, white, blue, green, orange, red;
    GradientDrawable test;
    TextView title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_about);
        black = (ImageView)findViewById(R.id.imgViewBlack);
        blue = (ImageView)findViewById(R.id.imgViewBlue);
        green = (ImageView)findViewById(R.id.imgViewBlue);
        red = (ImageView)findViewById(R.id.imgViewRed);
        orange = (ImageView)findViewById(R.id.imgViewOrng);
        white = (ImageView)findViewById(R.id.imgViewWhite);
        title = (TextView)findViewById(R.id.textViewTitle);

        try {
            title.setText(getString(R.string.app_name) + " " + getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName + " " + title.getText());
        } catch (PackageManager.NameNotFoundException e) {
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

}
