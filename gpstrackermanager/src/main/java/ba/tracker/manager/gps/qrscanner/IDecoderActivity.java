package ba.tracker.manager.gps.qrscanner;

import android.graphics.Bitmap;
import android.os.Handler;

import com.google.zxing.Result;

import ba.tracker.manager.gps.qrscanner.camera.CameraManager;

public interface IDecoderActivity {

    public ViewfinderView getViewfinder();

    public Handler getHandler();

    public CameraManager getCameraManager();

    public void handleDecode(Result rawResult, Bitmap barcode);
}
