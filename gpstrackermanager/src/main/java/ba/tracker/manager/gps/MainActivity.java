package ba.tracker.manager.gps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ba.tracker.manager.gps.maping.MultiDrawable;
import ba.tracker.manager.gps.model.Uredjaj;
import ba.tracker.manager.gps.util.DateHelper;
import ba.tracker.manager.gps.util.DeviceParser;
import ba.tracker.manager.gps.util.Utils;


/**
 * Main show Map with devices activity
 */
public class MainActivity extends Activity implements ClusterManager.OnClusterClickListener<Uredjaj>, ClusterManager.OnClusterInfoWindowClickListener<Uredjaj>, ClusterManager.OnClusterItemClickListener<Uredjaj>, ClusterManager.OnClusterItemInfoWindowClickListener<Uredjaj>  {
    // Google Map
    private GoogleMap googleMap;

    //Clustering
    private ClusterManager<Uredjaj> mClusterManager;

    //Strings
    private String search_url = "";//13AE-JJ7F
    private static final String TAG_NAME = "name";
    private static final String TAG_TIME = "date";
    private static final String TAG_LAT = "lat";
    private static final String TAG_LON = "lon";
    private static final String TAG_SPEED = "speed";

    // Preferences
    SharedPreferences sharedpreferences;
    public static final String KEY_ID = "id";
    public static final String MyPREFERENCES = "MyPrefs" ;

    // JSON variables
    JSONArray jsonArray = null;
    JSONObject c = null;

    // Marker helpers
    DateHelper convert = new DateHelper();
    Calendar time;
    String timeString;
    SimpleDateFormat format = new SimpleDateFormat("MMM dd HH:mm");
    String formatString;
    String brzinaString;

    // Bound map
    private LatLngBounds.Builder bounds = new LatLngBounds.Builder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        // then you use
        search_url = sharedpreferences.getString(KEY_ID, "");
        Log.w("Preference kod: ", search_url);

        super.onCreate(savedInstanceState);
        getActionBar().setDisplayShowTitleEnabled(false);
        setContentView(R.layout.activity_main);
        if(search_url.equals("")||search_url.length()!=9){
            Intent i = new Intent(MainActivity.this, CheckCodeActivity.class);
            finish();
            startActivity(i);
        }

        try {
            // Loading map
            initilizeMap();
            startTracking();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class UredjajRenderer extends DefaultClusterRenderer<Uredjaj> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        //private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;


        public UredjajRenderer() {
            super(getApplicationContext(), googleMap, mClusterManager);


            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);

            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            //mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            //mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            //int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            //mImageView.setPadding(padding, padding, padding, padding);
            //mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(Uredjaj uredjaj, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
            switch (uredjaj.style){
                case 1 :
                    mIconGenerator.setColor(Color.BLACK);
                    break;
                case 2 :
                    mIconGenerator.setStyle(IconGenerator.STYLE_WHITE);
                    break;
                case 3:
                    mIconGenerator.setStyle(IconGenerator.STYLE_BLUE);
                    break;
                case 4:
                    mIconGenerator.setStyle(IconGenerator.STYLE_GREEN);
                    break;
                case 5:
                    mIconGenerator.setStyle(IconGenerator.STYLE_ORANGE);
                    break;
                case 6:
                    mIconGenerator.setStyle(IconGenerator.STYLE_RED);
                    break;
                default:
                    mIconGenerator.setStyle(IconGenerator.STYLE_DEFAULT);
                    break;
            }
            mIconGenerator.setBackground(getResources().getDrawable(uredjaj.profilePhoto));
            //mImageView.setImageResource(uredjaj.profilePhoto);
            Bitmap icon = mIconGenerator.makeIcon(
                    uredjaj.title+
                            uredjaj.info);
            // mImageView.setImageBitmap(icon);
            // Bitmap bitmap = Bitmap.createScaledBitmap(icon,90,60,false);
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(uredjaj.title);

        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Uredjaj> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Uredjaj u : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                Drawable drawable = getResources().getDrawable(u.profilePhoto);
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    protected void startTracking() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        // then you use
        search_url = sharedpreferences.getString(KEY_ID, "");
        mClusterManager = new ClusterManager<Uredjaj>(this, googleMap);
        mClusterManager.setRenderer(new UredjajRenderer());
        googleMap.setOnCameraChangeListener(mClusterManager);
        googleMap.setOnMarkerClickListener(mClusterManager);
        googleMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        new MyTask().execute(search_url);


    }

    @Override
    public boolean onClusterClick(Cluster<Uredjaj> uredjajCluster) {
        String firstName = uredjajCluster.getItems().iterator().next().title;
        Toast.makeText(this, uredjajCluster.getSize() + getString(R.string.include) + firstName + ")", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Uredjaj> uredjajCluster) {

    }

    @Override
    public boolean onClusterItemClick(Uredjaj uredjaj) {
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Uredjaj uredjaj) {

    }
    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.map_error_nomap), Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.close) {
            alertClose();
        } else if (item.getItemId() == R.id.refresh) {
            if(testWiFi()){
                Intent intent = getIntent();
                finish();
                startActivity(intent);
                return true;
            }else{
                buildAlertMessageNoWiFi();
                return false;
            }
        }else if(item.getItemId() == R.id.about){
            Intent intent = new Intent(this,AboutActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Metoda prolazi kroz jsonArray i kreira Uredjaj koji dodaje u mClusterManager
     * Prethodno se sredjuju vrijeme i brzina
     */
    private void setMarkers(){

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                c = jsonArray.getJSONObject(i);

                timeString = c.getString(TAG_TIME);
                Log.w("Vrijeme string: ", timeString);
                time = convert.toCalendar(timeString);
                time.add(Calendar.HOUR, -1);
                Log.w("Covert toCalendar: ", time.getTime().toString());
                formatString = "\n"+format.format(time.getTime());
                Log.w("formatString: ", formatString);

                //  Converting SPEED to km/h    //
                double brzina = Double.parseDouble(c.getString(TAG_SPEED))*1.60934;

                brzinaString = String.valueOf(brzina);
                try {
                    int j = brzinaString.indexOf(".");
                    brzinaString = "\n"+brzinaString.substring(0,j+2);
                }catch (Exception e){
                    brzinaString = "\n"+brzinaString;
                }

                double lat = Double.parseDouble(c.getString(TAG_LAT));
                double lon = Double.parseDouble(c.getString(TAG_LON));

                //////////////////////////////////////////////////////////
                //      TIME AND SPEED COLORS                           //
                //      BLACK - NOT REGISTERED FOR OVER 24H         (1) //
                //      WHITE - NOT REGISTERED FOR OVER 6H          (2) //
                //      BLUE - NOT REGISTERED FOR OVER 1H           (3) //
                //      GREEN - REGULAR REGISTERED, SPEED 0         (4) //
                //      ORANGE - REGULAR REGISTERED, SPEED <60KM/H  (5) //
                //      RED - REGULAR REGISTERED, SPEED >60KM/H     (6) //
                //////////////////////////////////////////////////////////

                //IconGenerator iconFactory = new IconGenerator(this);
                Period period = new Period(convert.toJodaDateTime(timeString), DateTime.now());
                // Log.w("Period: ", String.valueOf(period.getDays())+ ' ' +String.valueOf(period.getHours()));
                if(period.getDays()>=1){
                    //iconFactory.setColor(Color.BLACK);
                    //iconFactory.setBackground(getResources().getDrawable(R.drawable.markerblacksmall));
                    mClusterManager.addItem(new Uredjaj(new LatLng(lat, lon), c.getString(TAG_NAME), formatString + brzinaString + getString(R.string.map_speed_suf), 1, R.drawable.ic_small_blck));
                }else{
                    if(period.getHours()>6 && period.getHours()<=24){
                        //iconFactory.setStyle(IconGenerator.STYLE_DEFAULT);
                        //iconFactory.setBackground(getResources().getDrawable(R.drawable.markerwhitesmall));
                        mClusterManager.addItem(new Uredjaj(new LatLng(lat,lon),c.getString(TAG_NAME),formatString+brzinaString+getString(R.string.map_speed_suf), 2, R.drawable.ic_small_white));
                    }else{
                        if(period.getHours()>1 && period.getHours()<=6){
                            //iconFactory.setStyle(IconGenerator.STYLE_BLUE);
                            //iconFactory.setBackground(getResources().getDrawable(R.drawable.markerbluesmall));
                            mClusterManager.addItem(new Uredjaj(new LatLng(lat, lon), c.getString(TAG_NAME), formatString + brzinaString + getString(R.string.map_speed_suf), 3, R.drawable.ic_small_blue));
                        }else{
                            if(brzina>2 && brzina<=60){
                                //iconFactory.setStyle(IconGenerator.STYLE_ORANGE);
                                //iconFactory.setBackground(getResources().getDrawable(R.drawable.markerorngsmall));
                                mClusterManager.addItem(new Uredjaj(new LatLng(lat, lon), c.getString(TAG_NAME), formatString + brzinaString + getString(R.string.map_speed_suf), 5, R.drawable.ic_small_orng));
                            }else{
                                if (brzina>60){
                                    //iconFactory.setStyle(IconGenerator.STYLE_RED);
                                    //iconFactory.setBackground(getResources().getDrawable(R.drawable.markerredsmall));
                                    mClusterManager.addItem(new Uredjaj(new LatLng(lat, lon), c.getString(TAG_NAME), formatString + brzinaString + getString(R.string.map_speed_suf), 6, R.drawable.ic_small_red));
                                }else{
                                    //iconFactory.setStyle(IconGenerator.STYLE_GREEN);
                                    //iconFactory.setBackground(getResources().getDrawable(R.drawable.markergreensmall));
                                    mClusterManager.addItem(new Uredjaj(new LatLng(lat, lon), c.getString(TAG_NAME), formatString + brzinaString + getString(R.string.map_speed_suf), 4, R.drawable.ic_small_green));
                                }
                            }
                        }
                    }
                }

                googleMap.setPadding(20, 80, 20, 10);
                bounds.include(new LatLng(lat, lon));



            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 100));
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        mClusterManager.cluster();

    }



    // My AsyncTask start...

    class MyTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            jsonArray = new DeviceParser().getData(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (null == jsonArray || jsonArray.length() == 0) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.code_error_net), Toast.LENGTH_SHORT)
                        .show();
            } else {
                setMarkers();
            }
        }
    }

    public boolean testWiFi(){

        if (Utils.isNetworkAvailable(MainActivity.this) && Utils.isOnline()) {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.map_load_msg, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
            return true;
        }else{
            return false;
        }
    }
    private void buildAlertMessageNoWiFi() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.konekcija, null);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(promptsView);
        alertDialog.setCancelable(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.confirmation), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.ic_launcher);
        alertDialog.show();
    }

    public void alertClose(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.closeapp));
        alertDialogBuilder
                .setMessage(getString(R.string.warningmsg))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.confirm),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                Process.killProcess(Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
