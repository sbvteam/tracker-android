package ba.tracker.manager.gps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Pattern;

import ba.tracker.manager.gps.qrscanner.CaptureActivity;
import ba.tracker.manager.gps.util.DeviceParser;
import ba.tracker.manager.gps.util.Utils;


public class CheckCodeActivity extends Activity implements OnClickListener, OnKeyListener {

    // Preferences
    SharedPreferences sharedpreferences;
    public static final String KEY_ID = "id";
    public static final String MyPREFERENCES = "MyPrefs" ;

    //View
    EditText editText;
    private Button loginButton, qrButton;
    private TextView message;
    String kod = "";
    boolean test = false;
    private static final Pattern sPattern = Pattern.compile("[\\w\\d]{4}-[\\w\\d]{4}");

    //Process handler
    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        kod = sharedpreferences.getString(KEY_ID, "");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_code);

        message = (TextView) findViewById(R.id.tvMessage);
        editText = (EditText) findViewById(R.id.editText);
        loginButton = (Button)findViewById(R.id.loginButton);
        qrButton = (Button) findViewById(R.id.butQR);

        //  UKOLIKO JE KOD UNESEN KROZ QR KOD, UPISUJEMO GA U EDITTEXT
        if(getIntent().hasExtra("kod")){
            kod = getIntent().getStringExtra("kod");
            editText.setText("");
            editText.setText(kod);
        }

        //  UKOLIKO KOD POSTOJI, PROVJERI DA LI JE ISPRAVAN
        if(!kod.isEmpty() || !kod.equals("")){
            editText.setText(kod);
            testing();
        }

        if(getIntent().hasExtra("msg")){
            Bundle extras = getIntent().getExtras();
            message.setText(extras.getString("msg"));
        } else{
            message.setText("");
        }

        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        loginButton.setOnClickListener(this);
        qrButton.setOnClickListener(this);

    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (v==loginButton) {
            if(editText.getText().toString().compareTo("")==0){
                Toast toast=Toast.makeText(getApplicationContext(), getString(R.string.code_error_message), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }else{
                testing();
            }
        }else{
            if(v==qrButton){
                Intent i = new Intent(CheckCodeActivity.this, CaptureActivity.class);
                startActivity(i);
            }
        }
    }

    public void testing(){
        if(editText.getText().toString().compareTo("")==0){
            Toast toast=Toast.makeText(getApplicationContext(), getString(R.string.code_error_message), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }else{
            if(sPattern.matcher(editText.getText().toString()).matches()){
                kod = editText.getText().toString();
                if(testWiFi()){
                    mHandler.postDelayed(new Runnable() {
                        public void run() {
                            new MyTask().execute(editText.getText().toString());
                        }
                    }, 2100);

                }else{
                    buildAlertMessageNoWiFi();
                }
            }else{
                Toast toast=Toast.makeText(getApplicationContext(), getString(R.string.code_error_length_msg), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
                editText.setText("");
            }
        }

    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return false;
    }

    class MyTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                test = new DeviceParser().getUserData(params[0]);
                return null;
            }catch (Exception e){
                Toast.makeText(getApplicationContext(),
                        getString(R.string.code_error_gen), Toast.LENGTH_SHORT)
                        .show();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!test) {

                Toast toast = Toast.makeText(getApplicationContext(), R.string.code_error_wrong, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                String s = getString(R.string.code_error_msg);

                Intent i = new Intent(getApplicationContext(),
                        CheckCodeActivity.class);
                i.putExtra("msg", s);
                // Closing all previous activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(i);
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.code_welcome), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();
                sharedpreferences.
                        edit().
                        putString(KEY_ID, kod).
                        commit();
                //iz nekog razloga ne moze da pokupi editText vrijednost...
                //pa stavljam direktno vrijednost iz koda...

                Intent i = new Intent(CheckCodeActivity.this,
                        MainActivity.class);
                // Closing all previous activities
                finish();
                toast.cancel();
                startActivity(i);

            }
        }
    }

    public boolean testWiFi(){

        if (Utils.isNetworkAvailable(CheckCodeActivity.this) && Utils.isOnline()) {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.code_verify_code, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
            return true;
        }else{
            return false;
        }
    }
    private void buildAlertMessageNoWiFi() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.konekcija, null);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(promptsView);
        alertDialog.setCancelable(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.confirmation), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.ic_launcher);
        alertDialog.show();
    }

}
