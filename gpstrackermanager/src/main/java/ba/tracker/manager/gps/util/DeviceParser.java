package ba.tracker.manager.gps.util;

import android.location.Location;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DeviceParser {

    boolean test = false;
    List<Location> listArray;

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    // url to get all vehicles list
    private static String url_status_check = "https://tracker.ba/api/v1/";
    private static String url_locations = "https://gps.tracker.ba/api/v1/";

    // JSON Node names
    private static final String TAG_SUCCESS = "status";
    private static final String TAG_LOCATIONS = "locations";

    // vehicles JSONArray
    JSONArray locations = null;
    JSONObject c = null;

    public JSONArray getData(String url){

        try {

            listArray = new ArrayList<Location>();

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            //Log.w("Kod je: ", url);
            url = url_locations +url+"/locations";
            JSONObject json = jParser.makeHttpRequest(url, "GET", params);
            // Check your log cat for JSON reponse
            //Log.w("All locations: ", json.toString());

            // Checking for SUCCESS TAG
            int success = json.getInt(TAG_SUCCESS);
            //Log.w("Status", String.valueOf(success));
            if(success==200){
                locations = json.getJSONArray(TAG_LOCATIONS);
            }

        } catch (JSONException e) {
            //Log.w("JsonExeption", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (Exception e) {
            //Log.w("Exeption", e.getLocalizedMessage());
            e.printStackTrace();
        }

        return locations;
    }


    public boolean getUserData(String url) {

        try {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            url = url_status_check +url+"/check";
            //Log.w("json url: ", url);
            JSONObject json = jParser.makeHttpRequest(url, "GET", params);

            // Check your log cat for JSON reponse
            //Log.w("Checking json: ", json.toString());


            // Checking for SUCCESS TAG
            int status = json.getInt(TAG_SUCCESS);

            if (status == 226)
            {
                // kod pronadjen
                // mozemo nastaviti dalje
                test = true;

            }

        } catch (JSONException e) {
            //Log.w("JsonExeption", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (Exception e) {
            //Log.w("Exeption", e.getLocalizedMessage());
            e.printStackTrace();
        }
        return test;
    }


}
