package ba.tracker.manager.gps.model;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by nikola on 30/12/14.
 */
public class Uredjaj implements ClusterItem {

    public final String title;
    public final String info; // vrijeme + brzina
    public final int style;
    public final int profilePhoto;
    private final LatLng mPosition;

    public Uredjaj(LatLng position, String title, String info, int style, int image) {
        this.title = title;
        this.info = info;
        this.style = style;
        profilePhoto = image;
        mPosition = position;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }
}
