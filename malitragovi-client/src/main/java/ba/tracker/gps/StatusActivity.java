/*
 * Copyright 2014 Tracker.ba
 */
package ba.tracker.gps;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

public class StatusActivity extends ListActivity {

    private static final int LIMIT = 20;

    private static final LinkedList<String> messages = new LinkedList<String>();
    private static final Set<ArrayAdapter<String>> adapters = new HashSet<ArrayAdapter<String>>();

    private static void notifyAdapters() {
        for (ArrayAdapter<String> adapter : adapters) {
            adapter.notifyDataSetChanged();
        }
    }

    public static void addMessage(String message) {
        //Log.i(MainPrefActivity.LOG_TAG, message);
        DateFormat format = DateFormat.getTimeInstance(DateFormat.SHORT);
        message = format.format(new Date()) + " - " + message;
        messages.addFirst(message);
        while (messages.size() > LIMIT) {
            messages.removeLast();
        }
        notifyAdapters();
    }

    public static void clearMessages() {
        messages.clear();
        notifyAdapters();
    }

    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, messages);
        setListAdapter(adapter);
        adapters.add(adapter);
    }

    @Override
    protected void onDestroy() {
        adapters.remove(adapter);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.status, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.clear) {
            clearMessages();
            return true;
        }else{
            if (item.getItemId() == R.id.tasks){
                Intent i = new Intent(getApplicationContext(),
                        TaskActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
                return true;
            }else{
                if (item.getItemId() == R.id.zadaci){
                    Intent i = new Intent(getApplicationContext(),
                            MainPrefActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(i);
                    return true;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
