package ba.tracker.gps;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import ba.tracker.gps.util.TaskParser;
import ba.tracker.gps.util.TextViewEx;


/**
 *
 */
public class DetailActivity extends Activity implements OnClickListener {

    Boolean test = false;
    Boolean myTaskType = false; // false - do seen; true - do completed;
    private String pid, kod;
    private TextView tvNaslov;
    private TextViewEx tvPoruka;
    private TextView tvScheduled;
    private Button zavrsiTask;
    private Bundle b;
    public static final String KEY_ID = "id";

    //Process handler
    Handler mHandler = new Handler();

    // Google Map
    private GoogleMap googleMap;

    // Bound map
    private LatLngBounds.Builder bounds = new LatLngBounds.Builder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayShowTitleEnabled(false);
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this);
        kod = sharedpreferences.getString(KEY_ID,"");

        setContentView(R.layout.activity_detail);

        tvNaslov = (TextView) findViewById(R.id.tvMessageTitle);
        tvPoruka = (TextViewEx) findViewById(R.id.tvMessage);
        tvScheduled = (TextView) findViewById(R.id.tvScheduled);
        zavrsiTask = (Button) findViewById(R.id.zavrsiTask);

        b = getIntent().getExtras();
        pid = String.valueOf(b.getInt("pid"));
        tvScheduled.setText(b.getString("scheduled"));
        tvNaslov.setText(b.getString("title"));
        tvPoruka.setText(b.getString("message"));
        zavrsiTask.setOnClickListener(this);

        try {
            // Loading map
            initilizeMap();

            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {

                    //googleMap.setMyLocationEnabled(true);
                    //Location test = googleMap.getMyLocation();

                    if(b.getDouble("lat")==0.00 && b.getDouble("lon")==0.00){
                        /*Log.w("MyLocation: ", test.getLatitude() + " " + test.getLongitude());
                        googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(test.getLatitude(), test.getLongitude()))
                                .title("Moja pozicija"));*/
                    }else{
                        googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(b.getDouble("lat"), b.getDouble("lon")))
                                .title(b.getString("title")));
                        bounds.include(new LatLng(b.getDouble("lat"), b.getDouble("lon")));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(b.getDouble("lat"), b.getDouble("lon")), 17));
                        googleMap.setPadding(5, 5, 5, 5);
                        googleMap.getUiSettings().setMapToolbarEnabled(false);
                        googleMap.getUiSettings().setZoomControlsEnabled(true);
                    }

                    //bounds.include(new LatLng(test.getLatitude(), test.getLongitude()));

                }
            });
            //Log.w("LatLon: ", b.getDouble("lat")+", "+b.getDouble("lon"));


        } catch (Exception e) {
            e.printStackTrace();
        }

        new MyTask().execute(kod, pid);

    }


    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.msg_map_error_nomap), Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.glavni, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tasks:
                Intent i = new Intent(getApplicationContext(),
                        TaskActivity.class);
                // Closing all previous activities
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
                return true;
            case R.id.status:
                Intent ii = new Intent(getApplicationContext(),
                        StatusActivity.class);
                // Closing all previous activities
                ii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(ii);
                return true;
            case R.id.settings:
                Intent iii = new Intent(getApplicationContext(),
                        MainPrefActivity.class);
                // Closing all previous activities
                iii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iii);
                return true;
        }
        return false;
    }


    @Override
    public void onClick(View v) {
        if (v == zavrsiTask) {
            myTaskType = true;
            //Log.w("DetailActivity: kod i pid: ", kod + " " + pid);
            new MyTask().execute(kod, pid);
        }
    }

    public void showToast(String msg) {

    }



    class MyTask extends AsyncTask<String, Void, Void> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(DetailActivity.this);
            pDialog.setMessage(getString(R.string.msg_komunikacija));
            pDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {
            if(myTaskType){
                test = new TaskParser().getCompletedTask(params[0], params[1]);
            }else{
                test = new TaskParser().getSeenTask(params[0], params[1]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (null != pDialog && pDialog.isShowing()) {
                pDialog.dismiss();
            }

            if (!test) {
                if(myTaskType){
                    Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.msg_task_coplete_failed), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    Intent i = new Intent(getApplicationContext(),
                            TaskActivity.class);
                    // Zatvaramo sve prethodne aktivnosti
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(i);
                }

            } else {
                if(myTaskType){

                    Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.msg_task_complete_success), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    mHandler.postDelayed(new Runnable() {
                        public void run() {
                            Intent i = new Intent(DetailActivity.this,
                                    TaskActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            // Zatvaramo sve prethodne aktivnosti
                            finish();
                            startActivity(i);
                        }
                    }, 2100);


                }

            }
        }
    }


}


