/*
 * Copyright 2014 Tracker.ba
 */
package ba.tracker.gps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AutostartReceiver extends BroadcastReceiver {
	
	public static final String LOG_TAG = "GPSTracker.AutostartReceiver";
 
    @Override
    public void onReceive(Context context, Intent intent) {
    	SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    	if (sharedPreferences.getBoolean(MainPrefActivity.KEY_STATUS, false)) {
    		context.startService(new Intent(context, MainPrefService.class));
    	}
    }

}
