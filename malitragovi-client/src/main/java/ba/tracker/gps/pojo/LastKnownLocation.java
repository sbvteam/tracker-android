package ba.tracker.gps.pojo;

import android.location.Location;

/**
 * Created by nikola on 10/03/15.
 */
public class LastKnownLocation {

    Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
