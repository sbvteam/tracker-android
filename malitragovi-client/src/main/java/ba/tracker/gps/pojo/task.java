package ba.tracker.gps.pojo;

import java.util.Date;

/**
 * Created by nikola on 04/03/15.
 */
public class Task {
    private Integer id;
    private String title;
    private String scheduled_at;
    private String message;
    private Double lat;
    private Double lon;
    private Boolean is_seen, is_completed;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getScheduled_at() {
        return scheduled_at;
    }

    public void setScheduled_at(String scheduled_at) {
        this.scheduled_at = scheduled_at;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Boolean getIs_seen() {
        return is_seen;
    }

    public void setIs_seen(Boolean is_seen) {
        this.is_seen = is_seen;
    }

    public Boolean getIs_completed() {
        return is_completed;
    }

    public void setIs_completed(Boolean is_completed) {
        this.is_completed = is_completed;
    }
}
