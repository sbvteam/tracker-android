/*
 * Copyright 2014 Tracker.ba
 */
package ba.tracker.gps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import java.util.regex.Pattern;

import ba.tracker.gps.qrscanner.CaptureActivity;
import ba.tracker.gps.util.KodParser;
import ba.tracker.gps.util.Tester;
import ba.tracker.gps.util.Utils;


public class CheckCodeActivity extends Activity implements OnClickListener, OnKeyListener {

    // Preferences
    SharedPreferences prefs;
    public static final String KEY_ID = "id";

    //Process handler
    Handler mHandler = new Handler();

    //View
    EditText editText;
    private Button loginButton, qrButton;
    private TextView message;
    String kod = "";
    boolean test = false;
    private static final Pattern sPattern = Pattern.compile("[\\w\\d]{8}");

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        // then you use
        kod = prefs.getString(KEY_ID, "");

        // Log.w("prefs: ", kod);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_code);

        message = (TextView) findViewById(R.id.tvMessage);
        editText = (EditText) findViewById(R.id.editText);
        loginButton = (Button)findViewById(R.id.loginButton);
        qrButton = (Button) findViewById(R.id.butQR);

        //  UKOLIKO JE KOD UNESEN KROZ QR KOD (CaptureActivity), UPISUJEMO GA U EDITTEXT
        if(getIntent().hasExtra("kod")){
            kod = getIntent().getStringExtra("kod");
            editText.setText("");
            editText.setText(kod);
            prefs.
                    edit().
                    putString(KEY_ID, kod).
                    commit();
        }

        //  UKOLIKO KOD POSTOJI, PROVJERI DA LI JE ISPRAVAN
        if(!kod.isEmpty() || !kod.equals("")){
            editText.setText(kod);
            testing();
        }
        Bundle extras;
        if(getIntent().hasExtra("msg")){
            extras = getIntent().getExtras();
            message.setText(extras.getString("msg"));
            message.setTextColor(getResources().getColor(R.color.about_red));
        } else{
            if(getIntent().hasExtra("msg1")){
                extras = getIntent().getExtras();
                message.setText(extras.getString("msg1"));
            }else {
                message.setText("");
            }
        }

        // Podesavanje tastature da se moze zatvoriti nakon unosa koda
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        loginButton.setOnClickListener(this);
        qrButton.setOnClickListener(this);

    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (v==loginButton) {
                testing();
        }else{
            if(v==qrButton){
                Intent i = new Intent(CheckCodeActivity.this, CaptureActivity.class);
                startActivity(i);
            }
        }
    }

    // Provjeravamo da li je kod ispravne duzine i pokrecemo MyTask
    public void testing(){
        if(editText.getText().toString().compareTo("")==0 ){
            Toast toast=Toast.makeText(getApplicationContext(), getString(R.string.code_error_message), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }else{
            if(sPattern.matcher(editText.getText().toString()).matches()){
                kod = editText.getText().toString();
                prefs.
                        edit().
                        putString(KEY_ID, kod).
                        commit();
                editText.setText(kod);

                Log.w("ba.tracker.check", "uneseni kod je: "+ kod +", a vrijednost textboxa je: "+ editText.getText().toString());
                Intent i = new Intent(CheckCodeActivity.this, Tester.class);
                startActivityForResult(i, 1);

            }else{
                Toast toast=Toast.makeText(getApplicationContext(), getString(R.string.code_error_length_msg), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
                editText.setText("");
            }
        }

    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return false;
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                final String pushKod = "user_" + kod;
                ParsePush.subscribeInBackground(pushKod, new SaveCallback() {
                    String push = "user_" + kod;
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            Log.w("ba.tracker.checkcode", "successfully subscribed to the broadcast channel as " + pushKod);
                        } else {
                            Log.e("ba.tracker.checkcode", "failed to subscribe for push", e);
                        }
                    }
                });
                Toast toast = Toast.makeText(getApplicationContext(), R.string.code_welcome, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();

                Intent i = new Intent(CheckCodeActivity.this,
                        MainPrefActivity.class);
                // Zatvaramo sve prethodne aktivnosti
                finish();
                startActivity(i);
            }
            if (resultCode == RESULT_CANCELED) {
                kod="";
                getIntent().putExtra("msg", getString(R.string.code_error_msg));
                finish();
                startActivity(getIntent());
            }
        }
    }//onActivityResult
}
