package ba.tracker.gps;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.util.List;

import ba.tracker.gps.pojo.Task;
import ba.tracker.gps.util.NewsRowAdapter;
import ba.tracker.gps.util.TaskParser;
import ba.tracker.gps.util.Utils;


public class TaskActivity extends Activity implements OnItemClickListener {

    List<Task> arrayOfList;
    ListView listView;
    String kod;
    public static final String KEY_ID = "id";

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this);
        kod = sharedpreferences.getString(KEY_ID,"");

        setContentView(R.layout.activity_task);

        listView = (ListView) findViewById(R.id.listview);
        listView.setOnItemClickListener(this);

        if (Utils.isNetworkAvailable(TaskActivity.this)) {
            new MyTask().execute(kod);
        } else {
            showToast("Nema mrezne konekcije!!!");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    class MyTask extends AsyncTask<String, Void, Void> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(TaskActivity.this);
            pDialog.setMessage(getString(R.string.msg_loading_active_tasks));
            pDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {
            arrayOfList = new TaskParser().getTasksList(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (null != pDialog && pDialog.isShowing()) {
                pDialog.dismiss();
            }

            if (null == arrayOfList) {
                showToast(getString(R.string.msg_no_data));
                Intent i = new Intent(getApplicationContext(),
                        MainPrefActivity.class);
                // Closing all previous activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            } else {
                setAdapterToListview();
            }
        }
    }

    public void setAdapterToListview() {
        NewsRowAdapter objAdapter = new NewsRowAdapter(TaskActivity.this,
                R.layout.row, arrayOfList);
        listView.setAdapter(objAdapter);
    }

    public void showToast(String msg) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tasks, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.status:
                Intent ii = new Intent(getApplicationContext(),
                        StatusActivity.class);
                // Closing all previous activities
                ii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(ii);
                return true;
            case R.id.settings:
                Intent iii = new Intent(getApplicationContext(),
                        MainPrefActivity.class);
                // Closing all previous activities
                iii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iii);
                return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Task item = arrayOfList.get(position);
        Intent intent = new Intent(TaskActivity.this, DetailActivity.class);
        intent.putExtra("pid", item.getId());
        intent.putExtra("scheduled", item.getScheduled_at());
        intent.putExtra("title", item.getTitle());
        intent.putExtra("message", item.getMessage());
        intent.putExtra("lat", item.getLat());
        intent.putExtra("lon",item.getLon());
        startActivity(intent);

    }
}
