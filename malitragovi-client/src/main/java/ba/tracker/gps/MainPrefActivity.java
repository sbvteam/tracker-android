/*
 * Copyright 2014 Tracker.ba
 */
package ba.tracker.gps;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import ba.tracker.gps.util.Utils;

/**
 * Main user interface
 */
@SuppressWarnings("deprecation")
public class MainPrefActivity extends PreferenceActivity {

    public static final String LOG_TAG = "GPSTracker.MainPrefActivity";

    public static final String KEY_ID = "id";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PORT = "port";
    public static final String KEY_INTERVAL = "interval";
    public static final String KEY_PROVIDER = "provider";
    public static final String KEY_EXTENDED = "extended";
    public static final String KEY_STATUS = "status";
    String kod = "";
    ImageView wifi;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayShowTitleEnabled(false);
        addPreferencesFromResource(R.xml.preferences);
        initPreferences(kod);



        ///////////////////////////////////////////////////////////////////////////////

        final SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
        setContentView(R.layout.activity_custom_pref);

        ///////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////
        Preference address = findPreference("address");
        getPreferenceScreen().removePreference(address);
        Preference port = findPreference("port");
        getPreferenceScreen().removePreference(port);
        Preference ext = findPreference("extended");
        getPreferenceScreen().removePreference(ext);
        kod = sharedPreferences.getString(KEY_ID,"");
        Log.w("Kod je: ", kod);
        ///////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////
        // Testiram WiFi
        wifi = (ImageView)findViewById(R.id.imageView);
        testWiFi();

        // Provjeravam GPS
        // testGps();
        ///////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////
        // Slusam promjenu statusa
        if (sharedPreferences.getBoolean(KEY_STATUS, false)) {
                startService(new Intent(this, MainPrefService.class));
        }
        ///////////////////////////////////////////////////////////////////////////////
    }


    @Override
    protected void onResume() {
        super.onResume();
        testWiFi();
        testGps();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(
                preferenceChangeListener);
    }

    @Override
    protected void onPause() {
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(
                preferenceChangeListener);
        super.onPause();
    }

    OnSharedPreferenceChangeListener preferenceChangeListener = new OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            testWiFi();
            if (key.equals(KEY_STATUS)) {
                if (sharedPreferences.getBoolean(KEY_STATUS, false)) {
                    startService(new Intent(MainPrefActivity.this, MainPrefService.class));
                } else {
                    stopService(new Intent(MainPrefActivity.this, MainPrefService.class));
                }
            } else if (key.equals(KEY_ID)) {
                findPreference(KEY_ID).setSummary(sharedPreferences.getString(KEY_ID, ""));
                final String pushKod = "user_" + kod;
                Log.w("Kod je: ", kod);
                ParsePush.unsubscribeInBackground(pushKod, new SaveCallback() {
                    String push = "user_" + kod;
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            Log.w("ba.tracker.main", "successfully unsubscribed to the broadcast channel as " + pushKod);
                        } else {
                            Log.e("ba.tracker.main", "failed to unsubscribe for push", e);
                        }
                    }
                });
                Intent i = new Intent(MainPrefActivity.this, CheckCodeActivity.class);
                i.putExtra("kod", sharedPreferences.getString(KEY_ID, ""));
                // Closing all previous activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(i);
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.status) {
            startActivity(new Intent(this, StatusActivity.class));
            return true;
        } else if (item.getItemId() == R.id.about) {
            startActivity(new Intent(this, AboutActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
            return true;
        }
        else if (item.getItemId() == R.id.tasks) {

            startActivity(new Intent(this, TaskActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (MainPrefService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void initPreferences(String id) {
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();

        if (!sharedPreferences.contains(KEY_ID) && !sharedPreferences.getString(KEY_ID,"").equals(id)) {
            sharedPreferences.edit().putString(KEY_ID, id).commit();
        }
        findPreference(KEY_ID).setSummary(sharedPreferences.getString(KEY_ID, id));
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.gpstest))
                .setCancelable(false)
                .setPositiveButton(R.string.confirmation, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getString(R.string.confirmation_negative), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void testWiFi(){
        if (Utils.isNetworkAvailable(MainPrefActivity.this) && Utils.isOnline()) {
            wifi.setImageResource(R.drawable.plainwifird);
        }else{
            buildAlertMessageNoWiFi();
            wifi.setImageResource(R.drawable.plainwifigr);
        }
    }
    public void testGps(){
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
    }
    private void buildAlertMessageNoWiFi() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.konekcija, null);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(promptsView);
        alertDialog.setCancelable(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.confirmation), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.ic_launcher);
        alertDialog.show();
    }
}


