package ba.tracker.gps;

import android.app.Application;
import android.os.Build;
import android.text.TextUtils;

import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by nikola on 04/03/15.
 */
public class ParsePushApplication extends Application {


    @Override
    public void onCreate(){

        // using static factory method from above
        // Log.w("ba.tracker.parsePush: ", "device using getDeviceName():  " + getDeviceName());
        // using the static factory method from Devices.java
        // Log.w("ba.tracker.parsePush: ", "device using Devices.java:  " + Devices.getDeviceName());
        // Log.w("ba.tracker.parsePush: ", "device os version:  " + getAndroidVersion());

        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_key));
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("deviceModel", getDeviceName());
        installation.put("osVersion", getAndroidVersion());
        installation.saveInBackground();


    }
    /** Returns the consumer friendly device name */
    public static String getDeviceName() {
        final String manufacturer = Build.MANUFACTURER;
        final String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        if (manufacturer.equalsIgnoreCase("HTC")) {
            // make sure "HTC" is fully capitalized.
            return "HTC " + model;
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        final char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (final char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    public String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return sdkVersion + " (" + release +")";
    }

}
