package ba.tracker.gps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import ba.tracker.gps.util.KodParser;
import ba.tracker.gps.util.Tester;
import ba.tracker.gps.util.Utils;


public class LaunchActivity extends Activity {

    // Preferences
    SharedPreferences prefs;
    public static final String KEY_ID = "id";
    String kod = "";
    boolean test = false;

    //Process handler
    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        // then you use
        kod = prefs.getString(KEY_ID, "");

        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        mHandler.postDelayed(new Runnable() {
            public void run() {
                Intent i = new Intent(LaunchActivity.this, Tester.class);
                startActivityForResult(i, 1);
            }
        }, 2100);


    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                final String pushKod = "user_" + kod;
                ParsePush.subscribeInBackground(pushKod, new SaveCallback() {
                    String push = "user_" + kod;

                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            Log.d("ba.tracker.launch", "successfully subscribed to the broadcast channel as " + pushKod);
                        } else {
                            Log.w("ba.tracker.launch", "failed to subscribe for push", e);
                        }
                    }
                });

                Intent i = new Intent(LaunchActivity.this,
                        MainPrefActivity.class);
                // Zatvaramo sve prethodne aktivnosti
                finish();
                startActivity(i);
            }
            if (resultCode == RESULT_CANCELED) {
                Intent i = new Intent(LaunchActivity.this,
                        CheckCodeActivity.class);
                i.putExtra("msg1", getString(R.string.msg_code_initial_msg));
                // Zatvaramo sve prethodne aktivnosti
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(i);
            }
        }
    }


}
