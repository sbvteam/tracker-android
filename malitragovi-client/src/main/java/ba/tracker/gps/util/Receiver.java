package ba.tracker.gps.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import ba.tracker.gps.CheckCodeActivity;
import ba.tracker.gps.TaskActivity;

/**
 * Created by nikola on 14/04/15.
 */
public class Receiver extends ParsePushBroadcastReceiver {

    // Preferences
    SharedPreferences prefs;
    public static final String KEY_ID = "id";
    String kod = "";
    boolean test = false;

    @Override
    public void onPushOpen(Context context, Intent intent) {
        Log.w("Push", "Clicked");

        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        // then you use
        kod = prefs.getString(KEY_ID, "");

        if(!kod.isEmpty() || !kod.equals("")){
            Intent i = new Intent(context, TaskActivity.class);
            i.putExtras(intent.getExtras());
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }else{
            Intent i = new Intent(context, CheckCodeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
}
