package ba.tracker.gps.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class KodParser {

	boolean test = false;

	// Creating JSON Parser object
	JSONParser jParser = new JSONParser();

	// url to get all vehicles list
	private static String url_status_check = "https://tracker.ba/api/v1/";

	// JSON Node names
	private static final String TAG_SUCCESS = "status";

	public boolean getUserData(String url) {

		try {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// getting JSON string from URL
            url = url_status_check +url+"/check";
            //Log.d("json url: ", url);
			JSONObject json = jParser.makeHttpRequest(url, "GET", params);

			// Check your log cat for JSON reponse
			//Log.d("Provjeravam json: ", json.toString());


			// Checking for SUCCESS TAG
			int status = json.getInt(TAG_SUCCESS);

			if (status == 200)
			{
				// kod pronadjen
				// mozemo nastaviti dalje
				test = true;

			}

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
        return test;
	}
	
	
}
