package ba.tracker.gps.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import ba.tracker.gps.R;

/**
 * Created by nikola on 15/04/15.
 */
public class Tester extends Activity{

    // Preferences
    SharedPreferences prefs;
    public static final String KEY_ID = "id";
    String kod = "";
    boolean test = false;
    //Process handler
    Handler mHandler = new Handler();
    Intent returnIntent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        // then you use
        kod = prefs.getString(KEY_ID, "");

        //  UKOLIKO KOD POSTOJI, PROVJERI DA LI JE ISPRAVAN
        if(!kod.isEmpty() || !kod.equals("")){

            if(testWiFi()){
                mHandler.postDelayed(new Runnable() {
                    public void run() {
                        new MyTask().execute(kod);
                    }
                }, 2100);

            }else{
                buildAlertMessageNoWiFi();
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            }

        }else{
            setResult(RESULT_CANCELED, returnIntent);
            finish();
        }


    }


    //Provjera jedinstvenog koda i vracanje rezultata provjere
    class MyTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.code_verify_code, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                test = new KodParser().getUserData(params[0]);
                return null;
            }catch (Exception e){
                Toast.makeText(getApplicationContext(),
                        getString(R.string.code_error_gen), Toast.LENGTH_SHORT)
                        .show();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!test) {

                Toast toast = Toast.makeText(getApplicationContext(), R.string.code_error_wrong, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                //iz nekog razloga ne moze da pokupi editText vrijednost...
                //pa stavljam direktno vrijednost iz koda...
                prefs.
                        edit().
                        putString(KEY_ID, "").
                        commit();

                // Return failed result to the parent intent
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            } else {
                // Return success result to the parent intent
                // Optional - you can add value to be passed
                // using putExtra option
                returnIntent.putExtra("result",true);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        }
    }
    public boolean testWiFi(){

        if (Utils.isNetworkAvailable(Tester.this) && Utils.isOnline()) {
            return true;
        }else{
            return false;
        }
    }
    private void buildAlertMessageNoWiFi() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.konekcija, null);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(promptsView);
        alertDialog.setCancelable(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.confirmation), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.ic_launcher);
        alertDialog.show();
    }


}
