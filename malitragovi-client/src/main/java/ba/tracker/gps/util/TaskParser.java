package ba.tracker.gps.util;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ba.tracker.gps.pojo.Task;

/**
 * Created by nikola on 04/03/15.
 */
public class TaskParser {

    boolean test = false;
    List<Task> listArray;

    // Task url list
    String url = "https://tracker.ba/api/v1/"; // http://tracker.ba/api/v1/CODE/tasks
    String list = "/tasks";
    String task = "/task/";
    String show = "/show"; //http://tracker.ba/api/v1/CODE/task/TASK_ID/show
    String completed = "/complete"; // http://tracker.ba/api/v1/CODE/task/TASK_ID/complete
    String code; // user_CODE
    String jsonUrl;

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    // JSON Node names
    private static final String TAG_SUCCESS = "status";
    private static final String TAG_TASKS = "tasks";
    private static final String TAG_PID = "id";
    private static final String TAG_TITLE = "title";
    private static final String TAG_SCH = "scheduled_at";
    private static final String TAG_MSG = "message";
    private static final String TAG_LAT = "lat";
    private static final String TAG_LON = "lon";
    private static final String TAG_SEEN = "is_seen";
    private static final String TAG_DONE = "is_completed";

    // vehicles JSONArray
    JSONArray taskList = null;
    JSONObject c = null;

    public List<Task> getTasksList(String code) {
        try {

            listArray = new ArrayList<Task>();

            Task task;

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            // getting JSON string from URL
            jsonUrl = url + code + list;
            Log.w("json url: ", jsonUrl);
            JSONObject json = jParser.makeHttpRequest(jsonUrl, "GET", params);

            // Check your log cat for JSON reponse
            //Log.w("Provjeravam json: ", json.toString());


            // Checking for SUCCESS TAG
            int status = json.getInt(TAG_SUCCESS);

            if (status == 200) {
                // lista postoji
                // mozemo nastaviti dalje
                taskList = json.getJSONArray(TAG_TASKS);

                for (int i = 0; i < taskList.length(); i++) {
                    c = taskList.getJSONObject(i);



                    // Storing each json item in variable

                    task = new Task();

                    task.setId(c.getInt(TAG_PID));
                    task.setTitle(c.getString(TAG_TITLE));
                    task.setScheduled_at(DateHelper.toCalendar(c.getString(TAG_SCH)).getTime().toString());
                    task.setMessage(c.getString(TAG_MSG));
                    try {
                        task.setLat(c.getDouble(TAG_LAT));
                        task.setLon(c.getDouble(TAG_LON));
                    }catch (Exception e){
                        task.setLat(0.00);
                        task.setLon(0.00);
                    }
                    task.setIs_seen(c.getBoolean(TAG_SEEN));
                    task.setIs_completed(c.getBoolean(TAG_DONE));

                    listArray.add(task);
                }
            } else {
                // NEMA TASKOVA
                // prazna lista
                return listArray;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listArray;
     }


    public boolean getSeenTask(String code, String TASKID) {


        try {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            jsonUrl = url+code+task+TASKID+show;
            Log.w("json seen url: ", jsonUrl);
            JSONObject json = jParser.makeHttpRequest(jsonUrl, "GET", params);

            // Check your log cat for JSON reponse
            Log.w("Provjeravam json: ", json.toString());


            // Checking for SUCCESS TAG
            int status = json.getInt(TAG_SUCCESS);

            if (status == 200)
            {
                // kod pronadjen
                // mozemo nastaviti dalje
                test = true;

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return test;
    }

    public boolean getCompletedTask(String code, String TASKID) {

        try {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            jsonUrl = url+code+task+TASKID+completed;
            //Log.d("json url: ", url);
            JSONObject json = jParser.makeHttpRequest(jsonUrl, "GET", params);

            // Check your log cat for JSON reponse
            //Log.d("Provjeravam json: ", json.toString());


            // Checking for SUCCESS TAG
            int status = json.getInt(TAG_SUCCESS);

            if (status == 200)
            {
                // kod pronadjen
                // mozemo nastaviti dalje
                test = true;

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return test;
    }

}
