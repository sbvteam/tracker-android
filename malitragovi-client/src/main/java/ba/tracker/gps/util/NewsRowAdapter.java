package ba.tracker.gps.util;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import ba.tracker.gps.R;
import ba.tracker.gps.pojo.Task;

/**
 * Created by nikola on 09/03/15.
 */
public class NewsRowAdapter extends ArrayAdapter<Task> {

    private Activity activity;
    private List<Task> items;
    private Task objBean;
    private int row;

    public NewsRowAdapter(Activity act, int resource, List<Task> arrayList) {
        super(act, resource, arrayList);
        this.activity = act;
        this.row = resource;
        this.items = arrayList;



    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(row, null);

            holder = new ViewHolder();
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if ((items == null) || ((position + 1) > items.size()))
            return view;

        objBean = items.get(position);

        holder.tvName = (TextView) view.findViewById(R.id.tvname);
        holder.tvDescription = (TextView) view.findViewById(R.id.tvdescription);
        holder.tvNewTask = (TextView) view.findViewById(R.id.tvNovo);

        if (holder.tvName != null && null != objBean.getTitle()
                && objBean.getTitle().trim().length() > 0) {
            holder.tvName.setText(Html.fromHtml(objBean.getTitle()));
        }
        if (holder.tvDescription != null && null != objBean.getMessage()
                && objBean.getMessage().trim().length() > 0) {
            holder.tvDescription.setText(Html.fromHtml(objBean.getMessage()));
        }
        if (holder.tvScheduled != null && null != objBean.getScheduled_at()
                && objBean.getScheduled_at().toString().trim().length() > 0) {
            holder.tvScheduled.setText(Html.fromHtml(objBean.getScheduled_at().toString()));
        }
        if (!objBean.getIs_seen()) {
            holder.tvNewTask.setVisibility(TextView.VISIBLE);
            holder.tvNewTask.setText(R.string.task_novo);
        }else {
            holder.tvNewTask.setVisibility(TextView.INVISIBLE);
        }

        return view;
    }

    public class ViewHolder {

        public TextView tvName, tvDescription, tvScheduled, tvNewTask;
        private ImageView imgView;
        private ProgressBar pbar;

    }

}
